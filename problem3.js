
 const modelNames=[];
function problem3(inventory)
{
    for(let i=0;i<inventory.length;i++)
    {
        modelNames[i]=inventory[i].car_model;
    }

    modelNames.sort(function(a,b) {
        a=a.toLowerCase();
        b=b.toLowerCase();
        if(a==b) return 0;
        if(a > b) return 1;
        return -1;
    });

    return modelNames;

}

module.exports=problem3;